# these are the aliases that are a part of the system.
alias dateme='date +%Y%m%dT%H%M%S'

# -- MINICOM aliases
alias minicom.mpa='/usr/bin/minicom -w -c on -C $HOME/logs/minicom.mpa_`dateme` precor.mpa'
alias minicom.cpa='/usr/bin/minicom -w -c on -C $HOME/logs/minicom.cpa_`dateme` precor.cpa'
alias minicom.usb0='/usr/bin/minicom -w -c on -C $HOME/logs/minicom.usb0_`dateme` usb0'
alias minicom.usb1='/usr/bin/minicom -w -c on -C $HOME/logs/minicom.usb1_`dateme` usb1'

# -- SUBVERSION aliases
alias svnprop_exe="svn propset svn:executable on"
alias svnprop_eol="svn propset svn:eol-style native"
alias svndiff='svn diff --diff-cmd /usr/bin/diff -x "-i -b"'

# -- QT Additions
#alias qt-4.5.2='export TROLLTECH=$HOME/workdir/tools/usr/local/Trolltech; source $HOME/.bash_aliases; export QT_VER=4.5.2; source $HOME/.bash_aliases'
#alias qt-4.6.0='export TROLLTECH=/usr/local/Trolltech/4.6.0; source $HOME/.bash_aliases; export QT_VER=4.6.0; source $HOME/.bash_aliases'
#alias qt-4.6.1='export TROLLTECH=/usr/local/Trolltech/4.6.1; source $HOME/.bash_aliases; export QT_VER=4.6.1; source $HOME/.bash_aliases'
#alias qt-4.6.2='export TROLLTECH=/usr/local/Trolltech/4.6.2; source $HOME/.bash_aliases; export QT_VER=4.6.2; source $HOME/.bash_aliases'

export TROLLTECH="/usr/local/Trolltech/$QT_VER"
alias qmake-6467="$TROLLTECH/Qt-6467/bin/qmake"
alias qmake-mv500="$TROLLTECH/Qt-6467/bin/qmake"

alias qmake-qvfb="$TROLLTECH/Qt-qvfb/bin/qmake"
alias qmake="$TROLLTECH/Qt-x11/bin/qmake"
alias qmake-x11="$TROLLTECH/Qt-x11/bin/qmake"
alias qmake-omap="$TROLLTECH/Qt-omap/bin/qmake"


# -- set environment
alias qt-x11='export PATH=$TROLLTECH/Qt-x11/bin:$ORIGPATH'
alias qt-qvfb='export PATH=$TROLLTECH/Qt-qvfb/bin:$ORIGPATH'
alias qt-6467='export PATH=$TROLLTECH/Qt-6467/bin:$ORIGPATH'
alias qt-omap='export PATH=$TROLLTECH/Qt-omap/bin:$ORIGPATH'
alias qvfb="$TROLLTECH/Qt-x11/bin/qvfb"
alias qvfb-precor="$TROLLTECH/Qt-x11/bin/qvfb -width 1024 -height 768 -depth 24 -skin $TOOLS/qt-precor-skin/precor.skin/"

alias l='ls -alh'
alias ls='ls -h --color=auto'

alias ..='cd ..'
alias md='mkdir -p'
alias rd='rmdir'
alias ff='find . -name'
alias ffi='find . -iname'

alias grep='grep --color=auto'
alias egrep='grep -E --color=auto'
alias fgrep='grep -F --color=auto'

alias alt='update-alternatives'
alias mountq='mount | column -t'


# general useful aliases
alias mkdir='mkdir -p'
alias which='type'
alias install='sudo apt-get install'
alias h='history'
alias j='jobs -l'
alias now='date +"%T"'
alias ping='ping -c 5'
alias df='df -h'
alias du='du -ch'
alias dus='du -chs'

# send stdout to clipboard
alias clip='xclip -sel clip'
# send stdout to clipboard and display it on stdout
alias tclip='xclip -sel clip && xclip -sel clip -o'

# tee cmd to display stdout on terminal as well as pipe it to something
alias tcon='tee /dev/tty'

alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd......='cd ../../../../..'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

alias .1='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'


# list hidden files
alias l.='ls -d .* --color=auto'
alias ll.='ls -l -d .* --color=auto'

# use wget in restartable mode
alias wget='wget -c'



# a hopefully easy to remember alias to fix PATH problems
alias repath='hash -r'

# I can never remember what stupid utility/options generate a usable hexdump
alias hex=hd

# I'm always mistyping 'git' as 'gti'.  Make bash automaticlaly fix this...
alias gti=git

# an easy to read/parse/grep display of all important network config info
alias netinfo='nmcli device show'
alias netconfig='nmcli device show'
alias ipaddr='nmcli device show | grep -E  "IP4\.ADDRESS|GENERAL\.TYPE"'

# open the  geany editor detached from the console
alias any='run geany'


# for some reason, the fastboot in the ./out/host/linux-x86/bin directory
#	(that lunch adds to the path) doesn't work to flash the P82 - at least
#	in my Linux development VM.  This adds an alias to a version of
#	fastboot that seems to work
alias fb='/home/precor/mybin/fastboot'

# - help build Peregrine
alias build_ota='$WORKDIR/precor/extras/sample_scripts/build_ota.sh'
alias build_firmware='$WORKDIR/precor/extras/sample_scripts/build_firmware_package.sh'

#
# use "-j 1" option to force repo status to be single threaded so it doesn't jumble lines of output
#
# note: repo-manifest-branch depends on the 'xpath' xml parsing utility (sudo apt-get install libxml-xpath-perl)
#
alias repo-checkout='repo_checkout'				# friendlier name than repo_checkout from .bash_aliases
alias dirty-repos='repo status -j 1 | grep -B 1 -v "^project "'
alias repo-dirty='repo status -j 1 | grep -B 1 -v "^project "'
alias repo-manifest-branch="xpath -q -e 'string(/manifest/default/@revision)' "'$(gettop)/.repo/manifests/default.xml'
alias repo-branches='echo "Manifest branch: $(repo-manifest-branch)" && '"repo forall -c 'git symbolic-ref --short HEAD' | sort | uniq"

#
# the following will list all projects followed by a single line that has that project's branch
#
# Here's an example that combines it with a somewhat complex regex that will output any projects
# that are not on the "cobra-6.2.4" branch:
#
#	repo-branches-verbose | grep -B 1 -E -v "^project|^cobra-6.2.4"
#
# Use the "-v" option with "forall" in the hope that it will make the output more likely
# to include everything necessary.
#
alias repo-branches-verbose="repo forall -v -p -c 'git symbolic-ref --short HEAD' | grep -v '^$'"


# this alias takes a regex and will list the repositories that are checked out to branches that match
alias repo-findbranch='repo forall -v -p -c '\''git symbolic-ref --short HEAD'\'' | grep -v '\''^$'\'' | grep -B 1'


# use the highlight function (in bash_funcs) to highlight things in stdout
#  but don't make me remember which way to spell it
alias hilite=highlight
alias hilight=highlight
alias highlite=highlight

# help find errors in builds
alias hilite-make-errs="highlight '^make: \*\*\* | error: '"
alias find-make-errs="grep -E -B 10 -A 1 '^make: \*\*\* | error: '"

# print path with each directory on its own line
#
# on the command line it would look like: echo "${PATH//:/$'\n'}"
#
# the byzantine quoting is due to needing both single and double quotes.
#
#       see: https://stackoverflow.com/a/1250279/12711
#
alias prtpath='echo "${PATH//:/$'"'"'\n'"'"'}"'


# automatcially trust an ssh host whose key signature has changed
# (consoles do this all the time)
alias sshx='ssh -o CheckHostIP=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=ERROR'
alias scpx='scp -o CheckHostIP=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=ERROR'


alias eclipse='detach $HOME/eclipse/eclipse'


# send a KEYCODE_MENU to an android devive to bring up the
# developer menu for a react-native app.  This way you don't
# have to actually shake the device.
alias android-shake='adb shell input keyevent 82'

# the adb & fastboot in the marshmallow build output don't work (at least not in a VM)
# the ones in the Android SDK's platform-tools directory do (v29 as of 2020-04-13)
alias adb='${HOME}/android/android-sdk-linux/platform-tools/adb'
alias fastboot='${HOME}/android/android-sdk-linux/platform-tools/fastboot'


#
# call the `mcd` function (defined in bash_funcs) - these aliases really exists only so that I can grep for it
#
alias mkdircd=mcd
alias mdcd=mcd


# trim trailing whitespace from lines in a file
# from http://ask.xmodulo.com/remove-trailing-whitespaces-linux.html
#
alias trimlines="sed -i 's/[[:space:]]*$//'"

alias reboot=$'echo "please use \'sudo reboot\'"'

alias cpus='lscpu --parse | grep -v "^#" | wc -l'

alias kwrite='kwrite 2>/dev/null'
